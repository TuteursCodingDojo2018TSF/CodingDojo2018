= Design de l'application

== Exemple de diagramme de classe de l'application

[plantuml, diag_class_files_det, format=svg, alt="Diagramme de classe détaillé"]
.Diagramme de classe détaillé
----
@startuml
skinparam handwritten true


package jeuDames.h <<Folder>> #cccccc {
    class Game
    class GameManager
    class Player
    class Board
    class Case
    Class iPiece <<interface>>
    Class Pion
    Class Dame 
    class iCaseInfos <<interface>> 
    enum PieceState {
       wait
       block
       dead
    }
    class BoardManager
}


interface iView {
	+getCaseInfos(Position case):caseInfos
}
iView --> Position
iView --> CaseInfos

interface iCmd {
	+isValid(Coup):CrCoup
	+doCoup(Coup):CrCoup
}
iCmd --> Coup
iCmd --> CrCoup

class Game <<Facade>> {
	+getCaseInfos(Position):caseInfos
	+isValid(Coup):CrCoup
	+doCoup(Coup):CrCoup
}
iCmd <|-- Game : implements
iView <|-- Game : implements


Game --> GameManager

GameManager *-- Player
GameManager --> "1" Player : Current

GameManager *-- "1" BoardManager
BoardManager --> Board

Player --> Color

Board *-- "50" Case : Black Cases

Case --> "0..1" iPiece

Pion --|> iPiece
Dame --|> iPiece

Board --|> iCaseInfos
iPiece --> iCaseInfos
iPiece --> PieceState
Case --> Position

iPiece --> Color
iPiece --> TypePiece


@enduml
----

