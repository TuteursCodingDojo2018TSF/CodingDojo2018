#include "gtest/gtest.h"
#include "gmock/gmock.h"


namespace
{    
    class VClass
    {
    public :
        virtual ~VClass() = default;
        
        virtual void call() = 0;
        virtual void callBis(int i) = 0;
    };
    
    
    class VClassMock : public VClass
    {
    public :
        
        MOCK_METHOD0(call, void());
		MOCK_METHOD1(callBis, void(int i));
    };
    
    
    int getValue()
    {
        return 5;
    }
    
    
    void callVClass(VClass& var)
    {
        var.call();
        var.callBis(10);
    }
}



TEST(TestSample, TestOK)
{
    ASSERT_EQ(5, getValue());
}


TEST(TestSample, TestKO)
{
    ASSERT_LT(100, getValue());
}


TEST(TestSample, TestMockOK)
{
    VClassMock mock;
    
    EXPECT_CALL(mock, call()).Times(1);
    EXPECT_CALL(mock, callBis(10)).Times(1);
    
    callVClass(mock);
}


TEST(TestSample, TestMockKO)
{
    VClassMock mock;
    
    EXPECT_CALL(mock, call()).Times(1);
    EXPECT_CALL(mock, callBis(6)).Times(1);
    
    callVClass(mock);
}


