project(CheckGameCLI)

include_directories(../includes)
file(GLOB   SRC_FILES  * )


add_executable(CheckGameCLI
	${SRC_FILES})
	
target_link_libraries(CheckGameCLI
	Game_sources)


