# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/GIT/PreparationCodingDojo2018/external/googletest/googletest/src/gtest-all.cc" "C:/GIT/PreparationCodingDojo2018/external/googletest/googlemock/CMakeFiles/gmock.dir/__/googletest/src/gtest-all.cc.obj"
  "C:/GIT/PreparationCodingDojo2018/external/googletest/googlemock/src/gmock-all.cc" "C:/GIT/PreparationCodingDojo2018/external/googletest/googlemock/CMakeFiles/gmock.dir/src/gmock-all.cc.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "external/googletest/googlemock/include"
  "external/googletest/googlemock"
  "external/googletest/googletest/include"
  "external/googletest/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
